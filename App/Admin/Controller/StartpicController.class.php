<?php
namespace Admin\Controller;
use Think\Controller;
class StartpicController extends CommonController {
	public function index()
    {
        $Startpic = M('Startpic');
        $map['mid'] = $this->mid;
        $map['appid'] = $this->appid;
        $volist = $Startpic->where($map)->select();
        $this->assign('volist',$volist);
        $this->display();
    }

}