
Lungo.Service.Settings.async = true;
Lungo.Service.Settings.error = function(type, xhr){
    Lungo.Notification.error('服务器错误', '', 'remove', 3);
};
Lungo.Service.Settings.headers["Content-Type"] = "application/json";
Lungo.Service.Settings.crossDomain = true;
// Lungo.Service.Settings.timeout = 30;

Lungo.Events.init({
    'load article'      : function(e) {
        // event.stopPropagation();
        // e.preventDefault();
    },
    'load article'      : function(e) {
        // console.log('t');        
    },
});


avalon.config({
    debug:false,
})

var Until = avalon.define("Until", function(vm){
    vm.urlParams = function (params) {
        var pars = {};
        pars['appid'] = appid;
        strs = params.split("&");
        for(var i=0;i<strs.length;i++)
        {
            var sTemp = strs[i].split("=");
            pars[sTemp[0]]=(sTemp[1]);
        }
        return  pars;
    }

    vm.navParams = function (params) {
        var pars = {};

        params = "c=Nav&"+params;

        pars['appid'] = appid;
        pars['limit'] = rows;
        pars['navId'] = navId;
        pars['a'] = 'getList';

        strs = params.split("&");
        for(var i=0;i<strs.length;i++)
        {
            var sTemp = strs[i].split("=");
            pars[sTemp[0]]=(sTemp[1]);
        }
        return  pars;
    }

    vm.viewParams = function (params) {
        var pars = {};

        pars['appid'] = appid;
        pars['a'] = 'view';
        pars['g'] = 'Rest';
        pars['m'] = 'Nav';

        strs = params.split("&");
        for(var i=0;i<strs.length;i++)
        {
            var sTemp = strs[i].split("=");
            pars[sTemp[0]]=(sTemp[1]);
        }
        return  pars;
    }
})


var duApp = avalon.define("duApp", function(vm){
    vm.currVo = {};
    vm.dataList = [];
    vm.CurrModule = '';
    vm.CurrNavid = '';


    vm.indexNavs = [];
    vm.asides = [];
    vm.isasides = false;
    vm.menus = [];
    vm.ismenus = false;
    vm.navs = [];
    vm.isnavs = false;

    vm.startPics = [];
    vm.iStartPics = false;

    vm.clickNav = function(el){
        // $$(this).addClass('active');
        vm.dataList.removeAll();
    }

    vm.openNav = function(el){
        var mod = el.module;
        var p = Until.navParams('navId='+el.id+"&page="+el.page);
        Lungo.Notification.show();
        Lungo.Service.json(restServer, p, function(resq){
            if (resq['volist']) {
                var vlist = resq['volist'];
                for (var i = 0; i < vlist.length; i++) {
                    vm.dataList.unshift(vlist[i]) ;
                };
            }
            Lungo.Notification.hide();
        });
        Lungo.Router.section(mod);
    }

    vm.openView = function(el){
        var mod = el.module+'-view';
        var p = Until.viewParams('viewId='+el.viewid+"&navId="+el.navId);
        Lungo.Notification.show();
        Lungo.Service.json(restServer, p, function(resq){
            vm.currVo = resq;
            Lungo.Notification.hide();
        });
        Lungo.Router.section(mod);
    }

})


require(["mmRouter"], function(avalon){

    



    avalon.router.get("/index", function() {
        var p = Until.urlParams('a=getNavList');
        Lungo.Service.json(restServer, p, function(resq){
            duApp.asides = resq['asides'];
            if (resq['asides'].length > 0 ) duApp.isasides = true;
            duApp.navs = resq['navs'];
            if (resq['navs'].length > 0 ) duApp.isnavs = true;
            duApp.menus = resq['menus'];
            if (resq['menus'].length > 0 ) duApp.ismenus = true;

            duApp.startPics = resq['startPics'];
            if (resq['startPics'].length > 0 ) duApp.iStartPics = true;

            duApp.indexNavs = resq['indexNavs'];

        });
    })

    

    avalon.router.get("/:s/:d(page/:bbb)", function(mod,navid,page) {
        var el = [];
        el['page'] = 1;
        if (typeof(page) != "undefined") el['page'] = page;
        el['module'] = mod;
        el['c'] = 'Nav';
        el['id'] = navid;
        duApp.CurrModule = mod;
        duApp.CurrNavid = navid;
        duApp.openNav(el);
    })

    avalon.router.get("/:s/:d/view/:d", function(mod,navid,id) {
        var el = [];
        el['module'] = mod;
        el['viewid'] = id;
        el['navId'] = navid;
        duApp.openView(el);
    })


    avalon.history.start({
        basepath: "/weiduniang_app"
    })
    avalon.router.navigate("/index")
    avalon.scan();
})

Lungo.ready(function() {

    $$('[data-pull]').each(function(){
        var pull = new Lungo.Element.Pull('[data-pull]>article.active', {
            onPull: "下拉刷新",
            onRelease: "释放获取新数据",
            onRefresh: "获取数据中...",
            callback: function() {
                var hash =  window.location.hash;
                hash = hash.replace('#!/','');
                var hash_arr = hash.split('/');
                var idx = hash_arr.indexOf("page");
                var page = 2;
                if (idx > 0 && hash_arr.length > idx) {
                    page = hash_arr[idx+1];
                    var opage = page;
                    page = parseInt(page) + 1;
                    hash = '/' + hash.replace('page/'+opage,'page/'+page);
                }else{
                    hash = "/"+hash+"/page/" + page;
                }
                avalon.router.navigate(hash);
                location.hash="#!"+hash;
                pull.hide();
            }
        });
    })

    

    

    // $$('[data-view-section][data-nav-id]').tap(function(e){
    //     Lungo.Router.section(sectionId);
    //     e.preventDefault();
    // })
});