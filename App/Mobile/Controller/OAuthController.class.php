<?php
namespace Mobile\Controller;
use Think\Controller;
class OAuthController extends CommonController {
    //Oath登录
    public function oathLogin($type = 'weixin')
    {
    	//回调地址 需要全路径
    	$callback = U('Mobile/OAuth/oathCallback?type='.$type,'',true,false,true);
		import("@.ORG.ThinkSDK.ThinkOauth");
		$oauth  = ThinkOauth::getInstance($type,'',$callback);
		// print_r($oauth->getRequestCodeURL());
		redirect($oauth->getRequestCodeURL());
    }

	//授权回调地址
    public function oathCallback($type = '',$code = '')
    {
        (empty($type) || empty($code)) && $this->error('参数错误');
		
		//加载ThinkOauth类并实例化一个对象
		import("@.ORG.ThinkSDK.ThinkOauth");
		$oauth  = ThinkOauth::getInstance($type);

		$token = $oauth->getAccessToken($code);

		cookie('token',$token);
		
		$oauth  = ThinkOauth::getInstance($type,$token);
		$ret = $oauth->call('userinfo');
		$uinfo = json_decode($ret, true);
    }
}