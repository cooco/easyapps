<?php
namespace Rest\Controller;
use Think\Controller;
class IndexController extends CommonController {


    //获取启动画面
    public function getStartPics()
    {
        $s_key = 's_startpic_'.$this->appid;
        if (S($s_key)) {
            return S($s_key);
        }
        $Startpic = D('Startpic');
        $map['appid'] = $this->appid;
        $startpics = $Startpic->getAll($map);
        S($s_key,$startpics);
        return $startpics;
    }
    
    //获取样式
    public function getStyles()
    {
        $s_key = 's_styles_'.$this->appid.'_'.$this->app['tpl'];
        if (S($s_key)) {
            $this->assign('styles',S($s_key));
            return;
        }
        $Styles = M('Styles');
        $map['tpl'] = $this->app['tpl'];
        $map['appid'] = $this->appid;
        $volist = $Styles->where($map)->select();
        $styles = array();
        foreach ($volist as $k) {
            if (!$k['content']) continue;

            if (strstr($k['cls'],'index-')) {
                $cls = str_replace("index-", '', $k['cls']);
                $cls = str_replace("font-", '', $cls);
                $styles['index'][$cls] = $k['content'];
            }elseif (strstr($k['cls'],'menu-')) {
                $cls = str_replace("menu-", '', $k['cls']);
                $cls = str_replace("font-", '', $cls);
                $styles['menu'][$cls] = $k['content'];
            }elseif (strstr($k['cls'],'aside-')) {
                $cls = str_replace("aside-", '', $k['cls']);
                $cls = str_replace("font-", '', $cls);
                $styles['aside'][$cls] = $k['content'];
            }elseif (strstr($k['cls'],'nav-')) {
                $cls = str_replace("nav-", '', $k['cls']);
                $cls = str_replace("font-", '', $cls);
                $styles['nav'][$cls] = $k['content'];
            }elseif (strstr($k['cls'],'title-')) {
                $cls = str_replace("title-", '', $k['cls']);
                $cls = str_replace("font-", '', $cls);
                $styles['title'][$cls] = $k['content'];
            }else{
                $cls = str_replace("title-", '', $k['cls']);
                $styles['global'][$cls] = $k['content'];
            }
        }
        // S($s_key,$styles);

        $this->assign('styles',$styles);
    }

    public function index()
    {
    	$data['indexNavs'] = $this->getIndexNav();
    	$data['asides'] = $this->getAside();
    	$data['menus'] = $this->getMenu();
        $data['navs'] = $this->getNav();
    	$data['startPics'] = $this->getStartPics();
    	$this->ajaxReturn($data);
    }


    public function getIndexNav()
    {
        $s_key = 's_index_nav_'.$this->appid;
        if (S($s_key)) {
        	return S($s_key);
            return;
        }
        $Nav = D('Nav');
        $map['show_index'] = 1;
        $map['appid'] = $this->appid;
        $Indexnavs = $Nav->getAll($map,'idx asc,id desc');
        S($s_key,$Indexnavs);
        return $Indexnavs;
    }

    public function getAside()
    {
        $s_key = 's_aside_'.$this->appid;

        if (S($s_key)) {
            return S($s_key);
        }
        $map['pos'] = 'aside';
        $map['appid'] = $this->appid;
        $Nav = D('Nav');
        $asides = $Nav->getAll($map,'idx asc,id desc');
        S($s_key,$asides);
        return $asides;
    }

    public function getMenu()
    {
        $s_key = 's_menu_'.$this->appid;
        if (S($s_key)) {
        	return S($s_key);
            return;
        }
        $Nav = D('Nav');
        $map['pos'] = 'menu';
        $map['appid'] = $this->appid;
        $menus = $Nav->getAll($map,'idx asc,id desc');
        S($s_key,$menus);
        return $menus;
    }

    public function getNav()
    {
        $s_key = 's_nav_'.$this->appid;

        if (S($s_key)) {
        	return S($s_key);
            return;
        }
        $Nav = D('Nav');
        $map['pos'] = 'bottom';
        $map['appid'] = $this->appid;
        $navs = $Nav->getAll($map,'idx asc,id desc');
        S($s_key,$navs);
        return $navs;
    }

    
}